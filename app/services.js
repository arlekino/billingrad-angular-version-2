

angular.module('billingrad')

.factory( 'Companies', function($routeParams, $firebase, cfpLoadingBar){

    var ref = new Firebase('https://burning-fire-2197.firebaseio.com/companies');
    var sync = $firebase(ref);
    var Companies = sync.$asArray();

    Companies.get = function(id) {
      var CompanyIndex = Companies.$indexFor(id);
      return Companies[CompanyIndex];
    }
    
    Companies.add = function(newdata) {
      return this.$add(newdata);
    }
    
    Companies.save = function(id) {
      var CompanyIndex = Companies.$indexFor(id);
      return this.$save(CompanyIndex);
    }
    
    Companies.remove = function(id) {
      var CompanyIndex = Companies.$indexFor(id);
      return this.$remove(CompanyIndex);
    }


    Companies.load = function() {
      var fn = function(){};
      
      if( arguments.length )
        fn = arguments[0];
        
      cfpLoadingBar.start();
      this.$loaded().then(function(){
        fn();
        cfpLoadingBar.complete();
      }, function(err) {
        console.error(err);
      });
    }

    return Companies;
})

.factory( 'Projects', function($routeParams, $firebase, cfpLoadingBar){
    
    var ref = new Firebase('https://burning-fire-2197.firebaseio.com/projects');
    var sync = $firebase(ref);
    var Projects = sync.$asArray();

    Projects.get = function(id) {
      var ProjectIndex = Projects.$indexFor(id);
      return Projects[ProjectIndex];
    }
    
    Projects.add = function(newdata) {
      return this.$add(newdata);
    }
    
    Projects.save = function(id) {
      var ProjectIndex = Projects.$indexFor(id);
      return this.$save(ProjectIndex);
    }
    
    Projects.remove = function(id) {
      var ProjectIndex = Projects.$indexFor(id);
      return this.$remove(ProjectIndex);
    }
    
    Projects.load = function() {
      var fn = function(){};
      
      if( arguments.length )
        fn = arguments[0];
        
      cfpLoadingBar.start();
      this.$loaded().then(function(){
        fn();
        cfpLoadingBar.complete();
      }, function(err) {
        console.error(err);
      });
    }
    
    return Projects;
})


.factory('Profile', function($routeParams, $firebase, cfpLoadingBar){
  var ref = new Firebase('https://burning-fire-2197.firebaseio.com/profile');
  var sync = $firebase(ref);
  var Profile = sync.$asArray();


  Profile.getUser = function() {
    var UserIndex = Profile.$indexFor('user');
    return Profile[UserIndex];
  }

  Profile.saveUser = function() {
    console.log( 'save 2' );
    var UserIndex = Profile.$indexFor('user');
    return this.$save(UserIndex);
  }

  Profile.load = function() {
    var fn = function(){};

    if( arguments.length )
      fn = arguments[0];

    cfpLoadingBar.start();
    this.$loaded().then(function(){
      fn();
      cfpLoadingBar.complete();
    }, function(err) {
      console.error(err);
    });
  }

  return Profile;
})
