angular.module('billingrad')

  .directive('pluginCheck', function(){
    return {
      restrict: 'A',
			link: function(scope, element, attrs) {
				$(element).iCheck();
			}
    }
  })
	.directive('pluginSlides', function() {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				
        $('.nav-links > div').on('click', function(){
          $('.nav-links').attr('data-active', $(this).index()+1 );
          $('.slides').attr('data-active', $(this).index()+1 );
				});
			}
		}
	})
	.directive('pluginDatepicker', function(){
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				$(element).datepicker({
					format: 'dd.mm.yyyy',
					weekStart: 1,
				});
			}
		}
	})
	.directive('pluginDropdown', function(){
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				$(element).dropdown({});
			}
		}
	})
	.directive('pluginAutosize', function(){
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				$(element).autosize({});
			}
		}
	})
	.directive('pluginTooltip', function(){
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				$(element).tooltip({});
			}
		}
	})
	.directive('pluginChosen', function(){
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				$(element).chosen(scope.$eval(attrs.pluginChosen));
			}
		}
	})
  .directive('previewUploaded', function(){
    return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				$(element).bind('change', function()
        {
         var file = this.files[0],
             reader = new FileReader();
             reader.onload = function(e)
             {
               $('.preview-uploaded > img').attr('src', e.target.result);
               console.log(e);
             }

         reader.readAsDataURL(file);
        });
			}
		}
  })

  .directive('pluginMask', function(){
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function(scope, element, attrs, ctrl) {
        var result = $(element).mask(attrs.pluginMask);
        return result[0].value;
      }
    }
  })

	.directive('companyForm', function () {
	        return {
	            restrict: 'EA',
	            templateUrl: 'app/assets/company-form.html',
	            controller: 'company'
	        };
	})
	
	.directive('avatar', function () {
    return {
      restrict: 'E',
      scope: { class: '@', src: '@', size: '@' },
      template: '<span class="avatar {{class}}"><img src="{{src}}" width="{{size}}" height="{{size}}" alt=""></span>'
    };
	})

  .directive('media', function() {
    return {
      restrict: 'E',
      transclude: true,
      scope: { class: '@', src: '@', size: '@' },
      controller: function($scope) {
        console.log( $scope );
        $scope.src = '/img/avatar-user.png';
      },
      template: '<div class="media">'+
                  '<div class="pull-left"><span class="avatar {{class}}">'+
                    '<img src="{{src}}" style="width: 100%; height: 100%; max-width: {{size}}px; max-height: {{size}}px;" width="{{size}}" height="{{size}}" alt="">'+
                  '</span></div>'+
                    '<ng-transclude></ng-transclude>'+
                '</div>'
    };
  })
  .directive('mediaBody', function(){
    return {
      require: '^media',
      restrict: 'E',
      transclude: true,
      replace: true,
      scope: { title: '@' },
      template: '<div class="media-body"><h2 class="media-heading">{{title}}</h2>'+ 
                '<ng-transclude></ng-transclude></div>'
    };
  })

	.directive('avatarSidebar', function () {
	        return {
	            restrict: 'EA',
	            templateUrl: 'app/assets/avatar-sidebar.html',
	            controller: function($scope, $route, $routeParams, $location, $firebase) {
		            var dataRef = new Firebase("https://burning-fire-2197.firebaseio.com/");
					$scope.all = $firebase(dataRef);
	            }
	        };
	})
