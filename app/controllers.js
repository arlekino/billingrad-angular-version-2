angular.module('billingrad')

.controller('AuthUser', function($scope, $route, $routeParams, $location, $firebase){
  $scope.navhide = true;
  $scope.sidebar = '';
  $scope.template = 'app/pages/auth.html';
})

.controller('controllerCompanies', function($scope, $route, $routeParams, $location, $firebase, Companies){

  $scope.currentCompany = $routeParams.comname;
  
  
  
  if( $routeParams.comname ) {
    Companies.load(function(){
        $scope.company = Companies.get( $routeParams.comname );
      })
    
    $scope.template = 'app/pages/company.html';
  }
  else {
    $scope.template = 'app/pages/create-company.html'; 
  }

  Companies.load(function(){
    $scope.companies = Companies;
  })
  
  
  $scope.save_company = function() {
    console.log( $scope.company );
    Companies.save($routeParams.comname);
  }
  
  $scope.remove_company = function() {
    Companies.remove($routeParams.comname).then(function(){
      $location.path('/companies');
    })
  }

  $scope.sidebar = 'app/sidebars/companies.html';

})

.controller('controllerNewCompany', function($scope, $route, $routeParams, $location, $firebase, Companies, Profile) {
  $scope.company = [];
  
  $scope.add_company = function() {
    $scope.company.stat = 'new';
    
    Companies.add($scope.company).then(function(){
      $location.path('/companies');
    })
  }

  $scope.user = Profile.getUser();

  $scope.sidebar  = '';
  $scope.template = 'app/pages/create-company.html';
})



.controller('AddressBook', function($scope, $route, $routeParams, $location, $firebase){
  var dataRef = new Firebase("https://burning-fire-2197.firebaseio.com/phonelists");
  $scope.phonelists = $firebase(dataRef);

  var dataRef = new Firebase("https://burning-fire-2197.firebaseio.com/user");
  $scope.user = $firebase(dataRef);

  $scope.sidebar = 'app/sidebars/addressbook.html';
  $scope.template = 'app/pages/phones.html';
})
  
.controller('SmsService', function($scope, $route, $routeParams, $location, $firebase){
  var dataRef = new Firebase("https://burning-fire-2197.firebaseio.com/phonelists");
  $scope.phonelists = $firebase(dataRef);

  var dataRef = new Firebase("https://burning-fire-2197.firebaseio.com/sms");
  $scope.smslists = $firebase(dataRef);

  var dataRef = new Firebase("https://burning-fire-2197.firebaseio.com/user");
  $scope.user = $firebase(dataRef);

  $scope.sidebar = 'app/sidebars/sms_service.html';
  $scope.template = 'app/pages/sms_service.html';
})
  
.controller('SmsServiceTariffs', function($scope, $route, $routeParams, $location, $firebase){
  var dataRef = new Firebase("https://burning-fire-2197.firebaseio.com/sms");
  $scope.smslists = $firebase(dataRef);

  $scope.sidebar = 'app/sidebars/sms_service.html';
  $scope.template = 'app/pages/sms_service_tariffs.html';
})




.controller('ApiDocs', function($scope, $route, $routeParams, $location) {
  $scope.sidebar = 'app/sidebars/api.html';
  $scope.template = 'app/pages/api.html';
})


.controller('controllerNewProject', function( $scope, $route, $routeParams, $location, $firebase, Projects ) {

  $scope.sidebar = '';
  $scope.template = 'app/pages/create-project.html';
})

.controller('controllerProjects', function($scope, $route, $routeParams, $location, $firebase, Projects) {

  $scope.currentProject = $routeParams.prjname;
  
  if( $routeParams.prjname ) {
    Projects.load(function(){
        $scope.project = Projects.get( $routeParams.prjname );
      })
    
    $scope.template = 'app/pages/project.html';
  }
  else {
    $scope.template = 'app/pages/create-project.html'; 
  }
  
  Projects.load(function(){
    $scope.projects = Projects;
  })

  $scope.sidebar = 'app/sidebars/projects.html';  
})


.controller('controllerProfile', function($scope, $route, $routeParams, $location, $firebase, Projects, Profile) {

  Profile.load(function(){
    $scope.user = Profile.getUser();
  })

  $scope.save_user = function() {
    Profile.saveUser();
  }
  
	//$scope.sidebar = 'app/sidebars/profile.html';
	$scope.template = 'app/pages/profile.html';
	
})