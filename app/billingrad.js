angular.module('billingrad', ['ngRoute', 'firebase', 'angular-loading-bar'])
	.config(function($routeProvider) {
	$routeProvider.
		when('/',            {templateUrl: 'app/pages/main.html', controller: 'controllerProfile' }).
    when('/profile',     {templateUrl: 'app/pages/main.html', controller: 'controllerProfile' }).
		when('/api',         {templateUrl: 'app/pages/main.html',  controller: 'ApiDocs' }).
  
    when('/companies',     {templateUrl: 'app/pages/main.html', controller: 'controllerCompanies'  }).
    when('/company/new', {templateUrl: 'app/pages/main.html', controller: 'controllerNewCompany'  }).
		when('/company/:comname', {templateUrl: 'app/pages/main.html', controller: 'controllerCompanies'  }).
  
		when('/addressbook', {templateUrl: 'app/pages/main.html', controller: 'AddressBook'  }).
		when('/sms_service', {templateUrl: 'app/pages/main.html', controller: 'SmsService'  }).
		when('/sms_service/tariffs', {templateUrl: 'app/pages/main.html', controller: 'SmsServiceTariffs'  }).		

    when('/projects',    {templateUrl: 'app/pages/main.html', controller: 'controllerProjects'  }).
		when('/project/new', {templateUrl: 'app/pages/main.html', controller: 'controllerNewProject'  }).
		when('/project/:prjname', {templateUrl: 'app/pages/main.html', controller: 'controllerProjects'  }).

		when('/auth', {templateUrl: 'app/pages/main.html', controller: 'AuthUser'  }).
		otherwise({redirectTo:'/'});

});



